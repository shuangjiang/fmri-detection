function Vg1a4=Synthact_maps(Q,Resumen,set)

% Esta funci�n devuelve 16 mapas de activaciones asociados al est�mulo 
% sint�tico s, para un conjunto de datos fMRI sint�ticos fijado.
% Los mapas de activaciones se presentan en una matriz 4x4, donde cada fila
% corresponde a un grupo, g1 a g4, y cada columna es una rebanada del
% volumen sint�tico dise�ado para tal fin.
%
% Entradas de la funci�n:
% - Q:      vector cuyas entradas se corresponden con los �ndices de los  
%           pixeles activados de manera sint�tica
% - Resumen: Estructura 1x4 con un campo tipo celda de nombre resumen
%            en el cual se almacene en un subcampo R el vector cuyas 
%            entradas se correspondan con los �ndices de los pixeles 
%            detectados como activados por el m�todo l0-LAD.
% 
%  Salida:
% - V: mapas de activaciones conformado por 16 rebanadas dispuestas en un 
%      arreglo de 4x4 im�genes (o matrices de tama�o 20x20 pixeles). 
%      A los pixeles detectados como activados se les asigna un valor de 
%      gris de 0.5, mientras que a las activaciones verdaderas se le asigna
%      el valor 1.

V=zeros(4,1600);
for group=1:4
    R=Resumen(1,group).resumen{1,set}.R; 
    v=zeros(1,1600);
    v(Q)=1; 
    v(R)=0.5;
    V(group,:)=v;
end
V_g1=reshape(V(1,:),[20,20,4]); 
V_g2=reshape(V(2,:),[20,20,4]); 
V_g3=reshape(V(3,:),[20,20,4]);
V_g4=reshape(V(4,:),[20,20,4]); 

Vg1=[V_g1(:,:,1), ones(20,1), V_g1(:,:,2), ones(20,1), V_g1(:,:,3) ones(20,1),...
    V_g1(:,:,4)];
Vg2=[V_g2(:,:,1), ones(20,1), V_g2(:,:,2), ones(20,1), V_g2(:,:,3) ones(20,1),...
    V_g2(:,:,4)];
Vg3=[V_g3(:,:,1), ones(20,1), V_g3(:,:,2), ones(20,1), V_g3(:,:,3) ones(20,1),...
    V_g3(:,:,4)];
Vg4=[V_g4(:,:,1), ones(20,1), V_g4(:,:,2), ones(20,1), V_g4(:,:,3) ones(20,1),...
    V_g4(:,:,4)];
Vg1a4=[Vg1; ones(1,83); Vg2; ones(1,83); Vg3; ones(1,83); Vg4];