close all;
% =========================================================================
%               ESTIMATORS BY USING DS
% =========================================================================

% loading estimation by using l0-LAD with a design matriz of 26 regressors
load('Beta(DS)_snorm_full_r4.mat')
% Generando las activaciones 
c=zeros(1,26);
c(6)=1;
g = 4;
z = [];
for set=1:20
    zt=Beta(1,g).estimates{1,set};
    z = [z zt];
end


for j=1:size(z,2)
    t(j)=c*z(:,j);
end
% choosing the 300 most significatives voxels 
u=sort(t,'descend');
u=u(300);
R=find(t>=u);

% loading functional volume and brain index
load('Vfunc_mo')
load('Brain_index')

V=zeros(64,64,34);
for k=1:34
    V(:,:,k)=Vfmo_SPM(:,:,k);
end
[Iva,V,XYZ] = Draw_activations_mod(V,Brain_index,R);
figure(1), clf
for i=1:length(Iva)
   figure;
   imshow(-V(:,:,Iva(1,i).Slice),[]), 
   title(['Slice ',num2str(Iva(1,i).Slice),' - GLM(DS) '],'FontWeight','demi')
   %pause
   %print -dpdf
end
 
% Select frames  and use reshape to create an array for montage
for k=1:size(V,3)
    Vmontage(:,:,k)=-V(:,:,k);
end
Vmontage = reshape(Vmontage(:,:,:),[size(V,1) size(V,2) 1 34]);
figure(2),montage(Vmontage,'DisplayRange',[],'Indices',1:34)%9:20
% saving the figure 2 (l0-LAD) for IEEE EMBS 
print -r300 -depsc 'activmap(DSr14)_1to34.eps'    % resolution at 300 dpi

% %% Extracci�n de los 300 voxeles activados por SPM  con stad�stico m�s
% % significativo para un SPM no corregido (uncorrected)
% load('xSPM_500none')
% u=sort(xSPM.Z,'descend');
% u=u(300);
% Index_Z=find(xSPM.Z>=u);   % almacena los �ndices de xSPM.Z>=u
% XYZ=xSPM.XYZ;
% XYZ_300=XYZ(:,Index_Z);    % Extrae coordenadas de los 300 voxeles seleccionados
% % carga imagen funcional Vfmo_SPM (radiological view L=R )
% load('Vfunc_mo')
% % Grafica sobre la imagen funcional
% V=zeros(64,64,34);
% for k=1:34
%     V(:,:,k)=Vfmo_SPM(:,:,k)';  % Para SPM es necesario transponer la imagen
% end
% intensity=2*max(Vfmo_SPM(:));
% for k=1:size(XYZ_300,2)
%     x=XYZ_300(1,k); y=(XYZ_300(2,k)); z=XYZ_300(3,k);
%     V(x,y,z)=intensity;
% end
% % genera volumen con 300 voxeles m�s significativos resaltados en
% % intensidad
% for k=1:size(V,3)
%     Vmontage(:,:,k)=-V(:,:,k)';
% end
% Vmontage = reshape(Vmontage(:,:,:),[size(V,1) size(V,2) 1 34]);
% figure(3),montage(Vmontage,'DisplayRange',[],'Indices',9:20)
% % saving the figure 2 (l0-LAD) for IEEE EMBS 
% print -r300 -depsc 'activmap(SPM300act)_9to20_300dpi.eps'    % resolution at 300 dpi