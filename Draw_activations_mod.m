function [Iva,V,XYZ] = Draw_activations_mod(V,Brain_index,R)

% Esta funci�n determina el �ndice de los voxeles activados respecto de 
% las posiciones originales
% Entrada:
%           V: Volumen fmri
% Brain_index: Estructura que contiene los �ndices de los voxeles dentro 
%              del cerebro para cada una de las rebanadas
%           R: Vector que contiene los �ndices de los voxeles activados 
%              respecto de la matriz de datos fmri As#r#         
% Salida
% Iva: Estructura que contiene el n�mero de la rebanda as� como los �ndices 
%      de los voxeles activados respecto de las posiciones
%      originales en cada una de las slices que conforman el volumen V sobre 
%      el cual se pintar�n las activaciones
% XYZ: Matriz de dimensi�n 3xlength(R), en cuyas filas se almacenan las 
%      coordenadas (x,y,z) de los voxeles activados   

Iva=struct('Slice',[],'Index',[]);
XYZ=zeros(3,length(R));
xdim=size(V,1); 
ydim=size(V,2);
intensity=2*max(V(:));
L=length(R); 
l=0;
slice=1;
R_new=R;
lb=0;
Nvxs=zeros(1,34);
i=1;            % Contador de rebanadas activadas
for k=1:34
    Nvxs(k)=size(Brain_index(1,k).Index,1); 
end
while (L>=1 && slice<35)
    ub=sum(Nvxs(1:slice));
    % Indice de voxeles activados en la slice activa
    iva=R_new(R_new>lb & R_new<=ub ); 
    liva=length(iva);
    if ~isempty(iva)
        Iva(i).Slice=slice;
        % Determina el �ndice del voxel en la rebanada # slice respecto de 
        % los  4096 voxeles que la conforman
        Iva(i).Index=Brain_index(1,slice).Index(iva-lb);
        vol=reshape(V(:,:,slice),1,4096);
        % Almacena las coordenadas de los voxeles activados en la matriz
        % xyz
        xyz=zeros(3,liva);
        xyz(1,:)=ceil((Iva(i).Index)/ydim);
        xyz(2,:)=Iva(i).Index'-((xyz(1,:)-1)*ydim);
        xyz(3,:)=ones(1,liva)*slice;
        XYZ(:,l+1:l+liva)=xyz;
        % Modifica la intensidad de los voxeles activados en vol
        vol(Iva(i).Index)=intensity;
        V(:,:,slice)=reshape(vol,64,64);
        i=i+1;
    end
    lb=ub; 
    l=l+length(iva);
    R_new=R(l+1:end);
    slice=slice+1;
    L=length(R_new);
end
% Coordenadas del voxel, conocido el n�mero del voxel
% x=ceil(Nvox/ydim);
% y=Nvox-(x-1)*ydim;

