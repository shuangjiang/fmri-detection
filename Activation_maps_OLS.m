% loading SPM mask
load('s14r1_mask')

% =========================================================================
%               ESTIMATORS BY USING l0-LAD
% =========================================================================

% loading estimation by using l0-LAD with a design matriz of 26 regressors
% load('s14r1(norm)_GLM(LAD)_X&DCT_SPMask')
load Beta(OLS)_snorm_full.mat;
% % Generando las activaciones
% c=zeros(size(Beta,1),1);
% ind_st=6;                       % index of Instruction in design matrix
% c(ind_st)=1;
% for j=1:size(Beta,2)
%     z(j)=c'*Beta(:,j);
% end
% % choosing the 300 most significatives voxels
% u=sort(z,'descend');
% u=u(300);
% R=find(z>=u);

% j = 1;
% z = [];
%  for i=1:20
%         z=[z Beta(1,j).activations{1,i}.statistics];
%  end
%
%  u=sort(z,'descend');
%  u=u(200);
%
%  R = [];
%  for i=1:20
%         z_new = Beta(1,j).activations{1,i}.statistics;
%         R = [R find(z_new>=u)+(i-1)*1600];
%  end



% or
c=zeros(1,26);
c(6)=1;
g = 1;
z = [];
for set=1:20
    zt=Beta(1,g).estimates{1,set};
    z = [z zt];
end


for j=1:size(z,2)
    t(j)=c*z(:,j);
end
% choosing the 300 most significatives voxels 
u=sort(t,'descend');
u=u(300);
R=find(t>=u);




% loading functional volume and brain index
load('Vfunc_mo')
load('Brain_index')

V=zeros(64,64,34);
for k=1:34
    V(:,:,k)=Vfmo_SPM(:,:,k);
end
[Iva,V,XYZ] = Draw_activations_mod(V,Brain_index,R);
figure(1), clf
for i=1:length(Iva)
    figure;
    imshow(-V(:,:,Iva(1,i).Slice),[]),
    title(['Slice ',num2str(Iva(1,i).Slice),' - GLM(LAD) '],'FontWeight','demi')
    %pause
    %print -dpdf
end

% Select frames  and use reshape to create an array for montage
for k=1:size(V,3)
    Vmontage(:,:,k)=-V(:,:,k);
end
Vmontage = reshape(Vmontage(:,:,:),[size(V,1) size(V,2) 1 34]);
figure(2),montage(Vmontage,'DisplayRange',[],'Indices',1:34)
% saving the figure 2 (l0-LAD) for IEEE EMBS
print -r300 -depsc 'activmap(OLS)_1to34.eps'    % resolution at 300 dpi

%% Extracci�n de los 300 voxeles activados por SPM  con stad�stico m�s
% significativo para un SPM no corregido (uncorrected)
load('xSPM_500none')
u=sort(xSPM.Z,'descend');
u=u(300);
Index_Z=find(xSPM.Z>=u);   % almacena los �ndices de xSPM.Z>=u
XYZ=xSPM.XYZ;
XYZ_300=XYZ(:,Index_Z);    % Extrae coordenadas de los 300 voxeles seleccionados
% carga imagen funcional Vfmo_SPM (radiological view L=R )
load('Vfunc_mo')
% Grafica sobre la imagen funcional
V=zeros(64,64,34);
for k=1:34
    V(:,:,k)=Vfmo_SPM(:,:,k)';  % Para SPM es necesario transponer la imagen
end
intensity=2*max(Vfmo_SPM(:));
for k=1:size(XYZ_300,2)
    x=XYZ_300(1,k); y=(XYZ_300(2,k)); z=XYZ_300(3,k);
    V(x,y,z)=intensity;
end
% genera volumen con 300 voxeles m�s significativos resaltados en
% intensidad
for k=1:size(V,3)
    Vmontage(:,:,k)=-V(:,:,k)';
end
Vmontage = reshape(Vmontage(:,:,:),[size(V,1) size(V,2) 1 34]);
figure(3),montage(Vmontage,'DisplayRange',[],'Indices',1:34)
% saving the figure 2 (l0-LAD) for IEEE EMBS
print -r300 -depsc 'activmap(SPM300act)_1to34_300dpi.eps'    % resolution at 300 dpi