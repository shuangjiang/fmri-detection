function [Beta,Param]=estimation_v1(Y,X,method,alfa,iter)

% This function implements the GLM to Virtual Reality data in order to detect
% activated voxels. Specifically, it resolves the problem:
%                       Y = X*beta
% where,
% - Y:       is the MxN matrix  that contains the fMRI data
% - X:       is the design matrix (Dictionary), it contains the stimuli,
% - method:  is an indicator for selecting regression method, it can be
%            'OLS', 'l1-ls' regression or 'LAD' regression
% - alfa:    tunning parameter for LAD method
% - iter:    number of iterations
% - Beta: is the vector of weights, i.e. the solution of problem

if nargin < 4, alfa=0.95; iter=100; end

Nvox=size(Y,2);
Nparam=size(X,2);
Beta=zeros(Nparam,Nvox);
switch method
    case {'OLS'} % uses OLS
        for j=1:Nvox
            y=Y(:,j);
            beta=pinv(X)*y;
            Beta(:,j)=beta;
        end
        Param=[];
    case {'DS'} % uses Double Sparsity Dictionary Learning
        Beta = zeros(500,Nvox);
        Beta= DS(Y);
        Param=[];
    case {'l1-ls'} % uses l1-ls method for regression
        rel_tol=0.01;
        Lambda=[];
        fprintf('       M�todo de regression seleccionado l1-ls\n')
        fprintf('---------------------------------------------------------\n')
        %porc=input('Ingrese el valor porc para lambda=porc*lamda_max ');
        %rel_tol=input('Ingrese el par�metro rel_tol ');
        porc=0.01;%0.02;%0.005;
        rel_tol=0.05; %1e-05;
        for j=1:Nvox
            y=Y(:,j);
            lambda_max=norm(2*X'*y,inf);
            lambda=porc*lambda_max;
            %Lambda=[Lambda lambda];
            [beta,status]=l1_ls(X,y,lambda,rel_tol,true);
            Beta(:,j)=beta;
            Param=lambda;
        end
    case {'LAD'}%else    %uses LAD regression
        Th_in=[];
        %fprintf('       M�todo de regression seleccionado LAD\n')
        %fprintf('---------------------------------------------------------\n')
        %alfa=input('Ingrese el valor del par�metro de entonaci�n ');
        %iter=input('Ingrese el n�mero de iteraciones ');
        epsilon = 1e-05;
        for j=1:Nvox
            y=Y(:,j);
            Thi=max(abs(X'*y));
            %Th_in=[Th_in Thi];
            [beta] = medrec_fa3_ultra_fast2(y,X,Thi,alfa,iter,epsilon);
            Beta(:,j)=beta;
        end
        Param=[alfa,iter];
end




