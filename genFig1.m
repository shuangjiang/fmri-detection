load s14r1_design_matrix_500.mat

beta = zeros(13,1);

beta(7) = 0.65;
beta(9) = 0.24;
beta(11) = 0.19;

Y = X_on*beta;

n = awgn(Y,10,'measured'); % Add white Gaussian noise.
Y = Y + n;

plot(Y)