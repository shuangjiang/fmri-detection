function Synthdata = synthdata_build(Y_na,xc,synthvol)

% This script allows to design 4 groups of 20 sets each one of synthetic 
% data by blending activation in non activated time-series that were
% extracted from the Virtual Reality dataset downloaded from PBAIC web
% site. Non activated time series belong to subject 14 run 1 dataset and 
% were clasified as non activated by the  SPM software respect to the
% instruction task by selecting the FWE mode.
%
% The synthetic time series in each voxel is given by
%               s(t) = b*xc(t) + y_nac(t)
% where b is the activation strength, xc is the convolved stimulus and
% y_nac(t) is a no activated time series.
%
% FORMAT  Synthdata = synthdata_build(Y_na,xc,synthvol)
% 
% Y_na     - data matrix which contains the non-activated time series 
% xc       - convolved stimulus, xc = h**x(t), where ** denotes the
%            convolution operator, h is the canonical hemodynamic filter
%            and x(t) is the raw (for example boxcar) stimulus
% synthvol - structure whith fields s1, s2, s3 and s4. Each one of these 
%            fields contain a binary mask for synthetic activations. 

Synthdata=struct('group',[],'b',[],'data',[]);
Ysint=cell(1,20);
b=[1 2 3 4];
% extracting positions of activated voxels in s1, s2, s3 and s4 masks
Im1=find(synthvol.s1); Im2=find(synthvol.s2); 
Im3=find(synthvol.s3); Im4=find(synthvol.s4);
for j=1:4
    f_v=b(j)*xc;                    % weigthed st�mulus by b
    for i=1:20
        % generating data matrix
        ci=1600*(i-1); cf=1600*i;   % first and last index in columns of Y
        Y=Y_na(:,(ci+1):cf);
        % activating voxels in non zero entries of masks
        Y(:,Im1)=Y(:,Im1)+repmat(f_v,1,25);
        Y(:,400+Im2)=Y(:,400+Im2)+repmat(f_v,1,42);
        Y(:,800+Im3)=Y(:,800+Im3)+repmat(f_v,1,24);
        Y(:,1200+Im4)=Y(:,1200+Im4)+repmat(f_v,1,17);
        Ysint{i}=Y;
    end
Synthdata(j).group=j;
Synthdata(j).b=b(j);
Synthdata(j).data=Ysint;
end

