
function Act=activations_v1(c,Beta,prob,type,dof,tmore)

% This function calculates the number of activations from the matrix Beta,
% which must contain the estimated parameters by using the method
% 'type', in the framework of GLM, for each one of the voxels belong 
% to the interest region to be studied. 
%  
%  
if nargin < 5, tmore=100; end
switch type
    case {'OLS'} % OLS t-student distribution
        t=zeros(1,(size(Beta,2)));
        for j=1:size(Beta,2)
            t(j)=(c*Beta(:,j))/(sqrt(var(Beta(:,j))*(c*c')));
        end
        t0=icdf('t',prob,dof);
        R=find(t>t0);
        %Act.method=type;
        Act.actvoxel=R;
        Act.statistics=t;
        Act.thresh=t0;
    case {'LAD'}% Laplacian distribution
        % Caso LAD 
        z=zeros(1,size(Beta,2));
        for j=1:size(Beta,2)
        z(j)=c*Beta(:,j);
        end
        mu=median(z);
        b=(1/length(z))*sum(abs(z-mu));
        theta=mu-b*sign(prob-0.5)*log(1-2*abs(prob-0.5));                              
        R=find(z>theta);        %Prueba de dos colas?
        %Act.method=type;
        Act.actvoxel=R;
        Act.statistics=z;
        Act.thresh=theta;
     case {'more'} % more significants
        z=zeros(1,size(Beta,2));
        for j=1:size(Beta,2)
            z(j)=c*Beta(:,j);
        end
        u=sort(z,'descend');
        u=u(tmore);
        if u > 0 
            R=find(z>=u);
        else
            R=find(z);
        end
        %R=find(z>=u);
        %Act.method=type;
        Act.actvoxel=R;
        Act.statistics=z;
        Act.thresh=u;
end