%% dise�o del boxcar del est�mulo, de la hrf y del est�mulo convolucionado 
% genera la funci�n boxcar del est�mulo (este paso es necesario solo si se
% desea determinar la se�al est�mulo cruda)
rand('seed',40)
onset=ceil(500.*rand(1,3)); % Genera 3 n�meros aleatorios enteros en el intervalo [0,500]
nscan=500; T=16; RT=1.75; dt=RT/T;
SPM.nscan=500; SPM.xBF.T=T; SPM.xBF.dt=dt;
U=spm_get_ons(SPM,1);
save('U','U')
% Generando la matriz de dise�o con SPM
design=spm_fMRI_design;     % genera el box car, la hrf y realiza la convoluci�n
save('stimuli_design','design')

% Convoluci�n de boxcar con hrf (cuando no se utiliza spm_fMRI_design)
x=spm_Volterra(U,bf,1);
xds=x([1:16:nscan*16]+32); % submuestreo



%% Generando el box car del est�mulo sin utilizar SPM
% duraci�n del est�mulo:
rand('state',79)
onset=ceil(55.*rand(1,3)); % Genera 3 n�meros aleatorios enteros en el intervalo [0,55]
%onset=onset(1:3);
d=6;                        % duraci�n del est�mulo
s=zeros(1,64);
for i=1:3
    s(onset(i):onset(i)+(d-1))=1;
end
figure(2), stem(s,'.')

% Genera la funci�n de respuesta hemodin�mica mediante SPM 
RT=1.75; RT=2;
[hrf,p]=spm_hrf(RT); 
eta=1;
s_conv=conv((eta*hrf),s);
figure(3), plot(s_conv);

% Genera data activada sint�tica
y=s_conv+Y_na(1:64,1);
figure(4), plot(y)

%  convoluci�n de la hrf con el est�mulo
eta=1;
y_act=conv((eta*hrf),s);
figure(3), plot(y_act);

bs=conv(SD.stimuli.data,SD.hrf.data);
figure(3),clf, plot(bs)

