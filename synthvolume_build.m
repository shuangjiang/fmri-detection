
%% Generando las m�scaras

% M�scara 1
s1=zeros(20,20); i=10; j=10;
for y=-3:0%3
    for x=-(3+y):(3+y)
        s1(i+x,j+y)=1;
        s1(i+x,j-y)=1;
    end    
end
synthetic(1,5).data{1,1}=s1;
subplot(2,2,1), imshow(s1)

% M�scara 2
s2=zeros(20,20); i=10; j=10;
for x=-3:3
    for y=-2:3
        s2(i+x,j+y)=1;
    end
end
synthetic(1,5).data{2,1}=s2;
subplot(2,2,2), imshow(s2)

% M�scara 3
s3=zeros(20,20); i=10; j=10; 
for y=-2:0
    for x=-(3+y):(3+y)
        %s3(i+x,j+y)=1;
        s3(i-x,j-y)=1;
        s3(i+x,j+3+y)=1;        
    end    
end
%s3=s3';
synthetic(1,5).data{3,1}=s3;
subplot(2,2,3), imshow(s3)

% M�scara 4
s4=zeros(20,20); i=10; j=10; 
for y=-3:0
    for x=-2*y:(4+y)
        s4(i+x,j+y)=1;
        %s4(i+2+x,j+3+y)=1;
        s4(j+3+y,i+2+x)=1;
        s4(j+3+y,i+2-x)=1;
    end    
    s4(j+3-y,i+2-x)=1;
end
synthetic(1,5).data{4,1}=s4;
subplot(2,2,4), imshow(s4)
save('syntheticdata_mask','s1','s2','s3','s4')

